#include "parser.h"

#include <iostream>
using namespace sl;

bool isWS(char c) {
  switch (c) {
    case ' ':
    case '\t':
    case '\n':
    case '\r':
      return true;
    default:
      return false;
  }
}

void ParserJson::_white() {
  while (ch != '\0' && isWS(ch)) {
    _next(0);
  }
}

void StringParserJson::_error(std::string m) {
  throw ParseException(m, at, text);
}

void StreamParserJson::_error(std::string m) {
  throw ParseException(m, at, "");
}

char StringParserJson::_next(char c) {
  if (c != 0 && c != ch) {
    std::string m = std::string("json syntax error :  caractère '") + c +
                    std::string("' attendu au lieu de '") + ch +
                    std::string("'");
    _error(m);
  }
  if (ch != '\0') {
    ch = text[at];
    at++;
  } else
    ch = '\0';
  return ch;
}

char StreamParserJson::_next(char c) {
  if (c != 0 && c != ch) {
    std::string m = std::string("json syntax error :  caractère '") + c +
                    std::string("' attendu au lieu de '") + ch +
                    std::string("'");
    _error(m);
  }
  if (!stream.eof()) {
    stream.get(ch);
    at++;
  } else
    ch = '\0';
  return ch;
}

ParseException::ParseException(std::string m, size_t at, const char* text)
    : message(m), eat(at), etext(text) {}

void ParseException::what() {
  if (!etext.empty()) {
    std::string str;
    size_t start = eat > 50 ? eat - 50 : 0;
    size_t end = eat < etext.size() - 50 ? eat + 50 : etext.size() - 1;
    std::string chunk = etext.substr(start, end - start + 1);
    std::cerr << chunk << std::endl;
    for (size_t i = start; i < eat - 1; i++) str += " ";
    str += "↑";
    std::cerr << str << std::endl;
  }
  std::cerr << message << " pos: " << eat << std::endl;
  // exit(1);
}

/*------------------------------------------------------------------------------
        Parsage de toute valeur JSON valide écrite sous forme de texte
-------------------------------------------------------------------------------*/

JsonValue* ParserJson::_value() {
  _white();
  switch (ch) {
    case L'{':
      return _object();
    case L'[':
      return _array();
    case L'\"': {
      return new JsonString(_string());
    }
    case L'-':
      return _number();
    default:
      if (L'0' <= ch && ch <= L'9')
        return _number();
      else {
        return _word();
      }
  }
}

JsonObject* ParserJson::_object() {
  auto object = new JsonObject();
  std::string key;
  if (ch == '{') {
    _next('{');
    _white();
    if (ch == '}') {
      _next('}');
      return object;  // object vide
    }
    while (ch != '\0') {
      key = _string();
      _white();
      _next(':');
      object->setValue(key, _value());
      _white();
      if (ch == '}') {
        _next('}');
        return object;
      }
      _next(',');
      _white();
    }
  }
  delete object;
  _error(std::string("json syntax error : '{' doit être refermée par '}'\n"));
  return nullptr;
}

JsonArray* ParserJson::_array() {
  JsonArray* array = new JsonArray();
  if (ch == '[') {
    _next('[');
    _white();
    if (ch == ']') {
      _next(']');
      return array;  // array vide
    }
    while (ch != '\0') {
      array->add(_value());
      _white();
      if (ch == ']') {
        _next(']');
        return array;
      }
      _next(',');
      _white();
    }
  }
  delete array;
  _error("json syntax error : '[' ouvrant doit être refermé par ']'\n");

  return nullptr;
}

std::string ParserJson::_string() {
  if (ch == '\"') {
    std::string parsedStr{};
    //  size_t start = at;
    //  size_t end = at;
    while (_next(0) != '\0') {
      if (ch == '"') {
        _next(0);
        // return std::string(text + start, end - start);
        return parsedStr;
      } else if (ch == '\\') {
        // std::cout << "caractère d'échappement\n";  //    TODO    caractères
        parsedStr += ch;
        _next(0);
        parsedStr += ch;
        // end += 2;
        //     d'échappement   //;
      } else {
        // end++;
        parsedStr += ch;
      }
    }
    _error(
        "json syntax error : chaîne de caractère incorrecte. [sans doute "
        "guillemets fermants manquants]\n");
  } else {
    std::string str = (ch == '\'') ? " et non simples (')\n" : "\n";
    _error(
        "json syntax error : une chaîne doit être délimitée par des guillemets "
        "doubles (\")" +
        str);
  }
  return "";
}

JsonValue* ParserJson::_number() {
  std::string str;
  if (ch == '-') {
    str = '-';
    _next('-');
  }
  while ('0' <= ch && ch <= '9') {
    str += ch;
    _next(0);
  }
  if (ch == '.') {
    str.push_back('.');
    while (_next(0) && ch >= '0' && ch <= '9') {
      str += ch;
    }
  }
  if (ch == 'e' || ch == 'E') {
    str += ch;
    _next(0);
    if (ch == '-' || ch == '+') {
      str += ch;
      _next(0);
    }
    while (ch >= '0' && ch <= '9') {
      str += ch;
      _next(0);
    }
  }
  return new JsonNumber(std::stod(str));
}

JsonValue* ParserJson::_word() {
  switch (ch) {
    case 't':
      _next('t');
      _next('r');
      _next('u');
      _next('e');
      return new JsonTrue();
    case 'f':
      _next('f');
      _next('a');
      _next('l');
      _next('s');
      _next('e');
      return new JsonFalse();
    case 'n':
      _next('n');
      _next('u');
      _next('l');
      _next('l');
      return new JsonNull();
    default:
      if (ch == '\'')
        _error(std::string(
            "json syntax error : une chaîne de caractères doit être "
            "délimitée par des guillements doubles (\") et non simples "
            "(\')\n"));
      else
        _error(
            std::string("json syntax error : la valeur parsée n'est pas une "
                        "valeur JSON valide.\n"));
      return nullptr;
      break;
  }
}
