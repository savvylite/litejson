/*------------------------------------------------------------------------------
    liteJSON: json.j
    Implémentation d'un parseur/generateur JSON
    Licence: GPLv3
    sviosdi - dernière mise à jour le 25.12.2022
-------------------------------------------------------------------------------*/

#ifndef SVL_JSON_H
#define SVL_JSON_H

#include <map>
#include <string>
#include <vector>

#include "json_utils.h"

namespace sl {

enum class JsonType {
  number = 0x01,
  string = 0x02,
  object = 0x03,
  array = 0x04,
  ok = 0x05,
  ko = 0x06,
  null = 0x0A
};

enum class Output { bash, simple };

class JsonObject;
class JsonValue;
class JsonArray;
class JsonNull;
struct ArrayAccessor;

struct ObjectAccessor {
  ObjectAccessor operator[](std::string);
  ArrayAccessor operator[](size_t idx);
  void operator=(double);
  void operator=(std::string);
  void operator=(JsonType tp);
  void operator=(JsonValue* v);
  std::string prettyStringify();
  std::string stringify();
  double vnumber();
  bool vbool();
  bool isNull();
  std::string& vstring();
  JsonType type();
  JsonObject* parent;
  JsonValue& selected;
  std::string key;
  static JsonNull nokey;  // Permet d'obtenir une référence vers une JsonValue
                          // indiquant que la clé n'a pas été trouvée.
};

struct ArrayAccessor {
  ObjectAccessor operator[](std::string);
  ArrayAccessor operator[](size_t);
  void operator=(std::string);
  void operator=(double d);
  void operator=(JsonType tp);
  void operator=(JsonValue* v);
  void add(JsonValue*);
  void add(const std::string&);
  void add(double);
  void add(JsonType);
  std::string prettyStringify();
  std::string stringify();
  double vnumber();
  bool vbool();
  bool isNull();
  std::string& vstring();
  JsonType type();
  JsonArray& array;
  size_t idx{0};
};

/*------------------------------------------------------------------------------
                            JsonValue
-------------------------------------------------------------------------------*/
class JsonValue {
 public:
  virtual JsonType type() const = 0;
  virtual ~JsonValue() {}
  inline char bsonCode() const { return (char)type(); }
  virtual std::string stringify() const = 0;
  virtual std::string prettyStringify(int ident = 0) const = 0;
  virtual bool equals(const JsonValue* other) const = 0;
  void setOutput(Output outp) { output = outp; }
  virtual ObjectAccessor operator[](std::string);
  virtual ArrayAccessor operator[](size_t idx);
  // les quatres méthodes add() suivantes ne sont utiles que pour JsonArray
  virtual void add(JsonValue*) {}
  virtual void add(const std::string&) {}
  virtual void add(double) {}
  virtual void add(JsonType) {}
  virtual uint32_t bsize() const = 0;
  virtual std::ostream& write(std::ostream& w) const = 0;
  virtual std::istream& read(std::istream& r);
  double vnumber();
  bool vbool();
  bool isNull();
  std::string& vstring();

 protected:
  static Output output;

 private:
  std::map<std::string, JsonValue*> items;
};

/*------------------------------------------------------------------------------
                            JsonObject
-------------------------------------------------------------------------------*/
class JsonObject : public JsonValue {
 public:
  ~JsonObject();
  JsonType type() const override { return JsonType::object; }
  std::string stringify() const override;
  void setValue(const std::string& key, JsonValue* val);
  JsonValue* getValue(const std::string& key) const;
  bool contains(const std::string& key) const;
  std::map<std::string, JsonValue*>& getItems() { return items; }
  std::string prettyStringify(int ident = 0) const override;
  bool equals(const JsonValue*) const override;
  ObjectAccessor operator[](std::string) override;
  uint32_t bsize() const override;
  std::ostream& write(std::ostream& w) const override;

 private:
  std::map<std::string, JsonValue*> items;
};

/*------------------------------------------------------------------------------
                            JsonArray
-------------------------------------------------------------------------------*/
class JsonArray : public JsonValue {
 public:
  ~JsonArray();
  JsonType type() const override { return JsonType::array; }
  std::vector<JsonValue*>& getElements() { return elements; }
  JsonValue* at(size_t);
  void setAt(size_t i, JsonValue* val);
  std::string stringify() const override;
  std::string prettyStringify(int ident = 0) const override;
  bool equals(const JsonValue*) const override;
  ArrayAccessor operator[](size_t idx) override;
  uint32_t bsize() const override;
  std::ostream& write(std::ostream& w) const override;
  std::istream& read(std::istream& r);
  void add(JsonValue*) override;
  void add(const std::string&) override;
  void add(double) override;
  void add(JsonType) override;

 private:
  std::vector<JsonValue*> elements;
};

/*------------------------------------------------------------------------------
                            JsonString
-------------------------------------------------------------------------------*/
class JsonString : public JsonValue {
 public:
  JsonString() : v_string{} {}
  JsonString(const std::string& str) : v_string{str} {}
  JsonType type() const override { return JsonType::string; }
  std::string& getValue() { return v_string; }
  void setValue(const std::string& str) { v_string = str; }
  std::string stringify() const override {
    return "\"" + sl::escapeChar(v_string) + "\"";
  }
  std::string prettyStringify(int ident = 0) const override;
  bool equals(const JsonValue*) const override;
  uint32_t bsize() const override;
  std::ostream& write(std::ostream& w) const override;
  virtual std::istream& read(std::istream& r);

 private:
  std::string v_string;
};

/*------------------------------------------------------------------------------
                            JsonNumber
-------------------------------------------------------------------------------*/
class JsonNumber : public JsonValue {
 public:
  JsonNumber(const double& d) : v_number{d} {}
  JsonType type() const override { return JsonType::number; }
  double& getValue() { return v_number; }
  void setValue(const double& d) { v_number = d; }
  std::string stringify() const override { return sl::showDouble(v_number); }
  std::string prettyStringify(int ident = 0) const override;
  bool equals(const JsonValue*) const override;
  uint32_t bsize() const override;
  std::ostream& write(std::ostream& w) const override;

 private:
  double v_number;
};

/*------------------------------------------------------------------------------
                            JsonTrue
-------------------------------------------------------------------------------*/
class JsonTrue : public JsonValue {
 public:
  JsonType type() const override { return JsonType::ok; }
  std::string stringify() const override { return "true"; }
  std::string prettyStringify(int ident = 0) const override;
  bool equals(const JsonValue*) const override;
  uint32_t bsize() const override;
  std::ostream& write(std::ostream& w) const override;
};

/*------------------------------------------------------------------------------
                            JsonFalse
-------------------------------------------------------------------------------*/
class JsonFalse : public JsonValue {
 public:
  JsonType type() const override { return JsonType::ko; }
  std::string stringify() const override { return "false"; }
  std::string prettyStringify(int ident = 0) const override;
  bool equals(const JsonValue*) const override;
  uint32_t bsize() const override;
  std::ostream& write(std::ostream& w) const override;
};

/*------------------------------------------------------------------------------
                            JsonNull
-------------------------------------------------------------------------------*/
class JsonNull : public JsonValue {
 public:
  JsonType type() const override { return JsonType::null; }
  std::string stringify() const override { return "null"; }
  std::string prettyStringify(int ident = 0) const override;
  bool equals(const JsonValue*) const override;
  uint32_t bsize() const override;
  std::ostream& write(std::ostream& w) const override;
};

/*------------------------------------------------------------------------------
                            JSON
-------------------------------------------------------------------------------*/
class JSON {
 public:
  static JsonValue* parse(const std::string&);
  static JsonValue* parseFile(const std::string&);
};

/*------------------------------------------------------------------------------
                            BSON
-------------------------------------------------------------------------------*/
class BSON {
 public:
  static JsonObject* readDocument(std::istream&);
};

}  // namespace sl
#endif  // SVL_JSON_H
