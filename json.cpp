#include "json.h"

#include <iostream>

#include "parser.h"
using namespace sl;

Output JsonValue::output{Output::simple};
JsonNull ObjectAccessor::nokey;
/*------------------------------------------------------------------------------
                        appel du parser de chaîne JSON
-------------------------------------------------------------------------------*/

JsonValue* JSON::parse(const std::string& s) {
  StringParserJson parser(s);

  try {
    if (s.empty()) {
      parser._error(
          "json syntax error : une chaîne vide ne peut pas être parsée.\n");
      return nullptr;
    }
    return parser._value();
  } catch (ParseException& e) {
    e.what();
    return nullptr;
  }
  return nullptr;
}

/*------------------------------------------------------------------------------
                        appel du parser de fichier
-------------------------------------------------------------------------------*/
JsonValue* JSON::parseFile(const std::string& filename) {
  std::ifstream stream(filename);
  try {
    if (!stream.is_open()) {
      std::cerr << "Erreur: le fichier '" << filename
                << "' n'a pas pu être ouvert.\n";
      return nullptr;
    }
    StreamParserJson parser(stream);
    JsonValue* v = parser._value();
    stream.close();
    return v;
  } catch (ParseException& e) {
    e.what();
    return nullptr;
  }
  return nullptr;
}

/*------------------------------------------------------------------------------
                        ObjectAccessor
-------------------------------------------------------------------------------*/
ObjectAccessor ObjectAccessor::operator[](std::string key) {
  if (selected.type() != JsonType::object)
    throw std::invalid_argument(
        "tentative d'accès par une clé sur une valeur qui n'est pas un objet");
  return ObjectAccessor{(JsonObject*)&selected,
                        *((JsonObject*)&selected)->getValue(key), key};
}

ArrayAccessor ObjectAccessor::operator[](size_t idx) {
  return {(JsonArray&)selected, idx};
}

void ObjectAccessor::operator=(double d) {
  if (selected.type() == JsonType::number) {
    ((JsonNumber*)&selected)->setValue(d);
  } else {
    JsonNumber* nb = new JsonNumber(d);
    parent->setValue(key, nb);
  }
}

void ObjectAccessor::operator=(std::string str) {
  if (selected.type() == JsonType::string) {
    ((JsonString*)&selected)->setValue(str);
  } else {
    JsonString* nstr = new JsonString(str);
    parent->setValue(key, nstr);
  }
}

void ObjectAccessor::operator=(JsonType tp) {
  switch (tp) {
    case JsonType::ok:
      parent->setValue(key, (JsonValue*)new JsonTrue());
      break;
    case JsonType::ko:
      parent->setValue(key, (JsonValue*)new JsonFalse());
      break;
    case JsonType::null:
      parent->setValue(key, (JsonValue*)new JsonNull());
      break;
    default:
      std::cerr << "json access error : Seuls les types JsonType::ok, ko et "
                   "null peuvent être assignés "
                   "par l'opérateur =. Sinon l'assignement sera sans effet."
                << std::endl;
      break;
  }
}

void ObjectAccessor::operator=(JsonValue* v) { parent->setValue(key, v); }

std::string ObjectAccessor::prettyStringify() {
  return selected.prettyStringify();
}

std::string ObjectAccessor::stringify() { return selected.stringify(); }

double ObjectAccessor::vnumber() {
  if (selected.type() == JsonType::number)
    return ((JsonNumber&)selected).getValue();
  throw std::invalid_argument(
      "json access error : Tentative d'accès par clé  à la valeur numérique "
      "d'une valeur qui est d'un autre type.");
}

bool ObjectAccessor::vbool() {
  if (selected.type() == JsonType::ko) return false;
  if (selected.type() == JsonType::ok) return true;
  throw std::invalid_argument(
      "json access error : Tentative d'accès par clé à la valeur booléenne "
      "d'une valeur qui est d'un autre type.");
}

bool ObjectAccessor::isNull() { return (selected.type() == JsonType::null); }

std::string& ObjectAccessor::vstring() {
  if (selected.type() == JsonType::string)
    return ((JsonString&)selected).getValue();
  throw std::invalid_argument(
      "json access error : Tentative d'accès  par clé  à la chaîne "
      "d'une valeur qui est d'un autre type.");
}

JsonType ObjectAccessor::type() { return selected.type(); }
/*------------------------------------------------------------------------------
                        ArrayAccessor
-------------------------------------------------------------------------------*/
ObjectAccessor ArrayAccessor::operator[](std::string key) {
  if (array.at(idx)->type() != JsonType::object) {
    throw std::invalid_argument(
        "json access error: Tentative d'accéder à la clé d'un élément de "
        "tableau qui n'est pas un objet\n");
  } else {
    if (((JsonObject*)array.at(idx))->contains(key))
      return ObjectAccessor{(JsonObject*)array.at(idx),
                            *((JsonObject*)array.at(idx))->getValue(key), key};
    return ObjectAccessor{(JsonObject*)array.at(idx), ObjectAccessor::nokey,
                          key};
  }
}

ArrayAccessor ArrayAccessor::operator[](size_t index) {
  if (array.at(idx)->type() != JsonType::array) {
    throw std::invalid_argument(
        "json access error: Tentative d'accéder par index à un élément de "
        "tableau "
        "qui n'est pas un tableau\n");
  } else {
    return ArrayAccessor{*((JsonArray*)array.at(idx)), index};
  }
}

void ArrayAccessor::operator=(std::string str) {
  array.setAt(idx, new JsonString(str));
}

void ArrayAccessor::operator=(double d) { array.setAt(idx, new JsonNumber(d)); }

void ArrayAccessor::operator=(JsonType tp) {
  switch (tp) {
    case JsonType::ok:
      array.setAt(idx, new JsonTrue());
      break;
    case JsonType::ko:
      array.setAt(idx, new JsonFalse());
      break;
    case JsonType::null:
      array.setAt(idx, new JsonNull());
      break;
    default:
      std::cerr << "json assign error: Seuls les types JsonType::ok, ko et "
                   "null peuvent être assignés "
                   "par l'opérateur =. Sinon l'assignement sera sans effet."
                << std::endl;
      break;
  }
}

void ArrayAccessor::operator=(JsonValue* v) { array.setAt(idx, v); }

void ArrayAccessor::add(JsonValue* v) {
  if (array.at(idx)->type() != JsonType::array)
    throw std::invalid_argument(
        "json access error: Tentative d'ajouter un élément à une valeur qui "
        "n'est pas un tableau.");
  array.at(idx)->add(v);
}

void ArrayAccessor::add(const std::string& str) {
  if (array.at(idx)->type() != JsonType::array)
    throw std::invalid_argument(
        "json access error: Tentative d'ajouter un élément à une valeur qui "
        "n'est pas un tableau.");
  array.at(idx)->add(new JsonString(str));
}

void ArrayAccessor::add(double d) {
  if (array.at(idx)->type() != JsonType::array)
    throw std::invalid_argument(
        "json access error: Tentative d'ajouter un élément à une valeur qui "
        "n'est pas un tableau.");
  array.at(idx)->add(new JsonNumber(d));
}
void ArrayAccessor::add(JsonType tp) {
  if (array.at(idx)->type() != JsonType::array)
    throw std::invalid_argument(
        "json access error: Tentative d'ajouter un élément à une valeur qui "
        "n'est pas un tableau.");
  switch (tp) {
    case JsonType::ok:
      array.at(idx)->add(new JsonTrue());
      break;
    case JsonType::ko:
      array.at(idx)->add(new JsonFalse());
      break;
    case JsonType::null:
      array.at(idx)->add(new JsonNull());
      break;
    default:
      std::cerr << "json add element error: Seuls les valeur de type "
                   "JsonType::ok, ko et null peuvent être ajoutés par"
                   " leur type à un tableau par add(). Sinon "
                   "l'ajout sera sans effet."
                << std::endl;
      break;
  }
}

std::string ArrayAccessor::prettyStringify() {
  return array.at(idx)->prettyStringify();
}

std::string ArrayAccessor::stringify() { return array.at(idx)->stringify(); }

double ArrayAccessor::vnumber() {
  JsonValue* v = array.at(idx);
  if (v->type() == JsonType::number) return ((JsonNumber*)v)->getValue();
  throw std::invalid_argument(
      "json access error : Tentative d'accès par indice  à la valeur numérique "
      "d'une valeur qui est d'un autre type.");
}

bool ArrayAccessor::vbool() {
  JsonValue* v = array.at(idx);
  if (v->type() == JsonType::ko) return false;
  if (v->type() == JsonType::ok) return true;
  throw std::invalid_argument(
      "json access error : Tentative d'accès par indice à la valeur booléenne "
      "d'une valeur qui est d'un autre type.");
}

bool ArrayAccessor::isNull() {
  return (array.at(idx)->type() == JsonType::null);
}

std::string& ArrayAccessor::vstring() {
  JsonValue* v = array.at(idx);
  if (v->type() == JsonType::string) return ((JsonString*)v)->getValue();
  throw std::invalid_argument(
      "json access error : Tentative d'accès  par indice  à la chaîne  "
      "d'une valeur qui est d'un autre type.");
}

JsonType ArrayAccessor::type() { return array.at(idx)->type(); }
/*------------------------------------------------------------------------------
                        JsonValue
-------------------------------------------------------------------------------*/
ObjectAccessor JsonValue::operator[](
    std::string) {  // ne devrait pas être appelée
  return ObjectAccessor{nullptr, *this, ""};
}
ArrayAccessor JsonValue::operator[](
    size_t idx) {  // ne devrait pas être appelée
  return ArrayAccessor{*((JsonArray*)this), idx};
}

double JsonValue::vnumber() {
  if (type() == JsonType::number) return ((JsonNumber*)this)->getValue();
  throw std::invalid_argument(
      "json  error : Tentative d'accès direct à la valeur "
      "numérique d'une valeur qui est d'un autre type.");
}

bool JsonValue::vbool() {
  if (type() == JsonType::ko) return false;
  if (type() == JsonType::ok) return true;
  throw std::invalid_argument(
      "json access error : Tentative d'accès direct à la valeur booléenne "
      "d'une valeur qui est d'un autre type.");
}

bool JsonValue::isNull() { return (type() == JsonType::null); }

std::string& JsonValue::vstring() {
  if (type() == JsonType::string) return ((JsonString*)this)->getValue();
  throw std::invalid_argument(
      "json access error : Tentative d'accès direct à la chaîne "
      "d'une valeur qui est d'un autre type.");
}

/*------------------------------------------------------------------------------
                        JsonNumber
-------------------------------------------------------------------------------*/
std::string JsonNumber::prettyStringify(int) const {
  if (output == Output::bash)
    return "\e[38;5;75m" + sl::showDouble(v_number) + "\e[0m";
  else if (output == Output::simple)
    return sl::showDouble(v_number);
  return "";
}

/*------------------------------------------------------------------------------
                        JsonString
-------------------------------------------------------------------------------*/
std::string JsonString::prettyStringify(int) const {
  if (output == Output::bash)
    return "\e[38;5;106m\'" + sl::escapeChar(v_string) + "\'\e[0m";
  else if (output == Output::simple)
    return '\'' + sl::escapeChar(v_string) + '\'';
  return "";
}

/*------------------------------------------------------------------------------
                        JsonTrue
-------------------------------------------------------------------------------*/
std::string JsonTrue::prettyStringify(int) const {
  if (output == Output::bash)
    return "\e[38;5;193mtrue\e[0m";
  else if (output == Output::simple)
    return "true";
  return "";
}

/*------------------------------------------------------------------------------
                        JsonFalse
-------------------------------------------------------------------------------*/
std::string JsonFalse::prettyStringify(int) const {
  if (output == Output::bash)
    return "\e[38;5;174mfalse\e[0m";
  else if (output == Output::simple)
    return "false";
  return "";
}

/*------------------------------------------------------------------------------
                        JsonNull
-------------------------------------------------------------------------------*/
std::string JsonNull::prettyStringify(int) const {
  if (output == Output::bash)
    return "\e[38;5;125mnull\e[0m";
  else if (output == Output::simple)
    return "null";
  return "";
}

/*------------------------------------------------------------------------------
                        implémentations de equals()
-------------------------------------------------------------------------------*/
bool JsonString::equals(const JsonValue* other) const {
  if (other->type() != JsonType::string)
    return false;
  else
    return v_string == ((JsonString*)other)->v_string;
}

bool JsonNumber::equals(const JsonValue* other) const {
  if (other->type() != JsonType::number) return false;
  return v_number == ((JsonNumber*)other)->v_number;
}

bool JsonTrue::equals(const JsonValue* other) const {
  return other->type() == JsonType::ok;
}

bool JsonFalse::equals(const JsonValue* other) const {
  return other->type() == JsonType::ko;
}

bool JsonNull::equals(const JsonValue* other) const {
  return other->type() == JsonType::null;
}

bool JsonObject::equals(const JsonValue* other) const {
  if (other == this) return true;
  if (other->type() != JsonType::object) return false;
  auto other_items = ((JsonObject*)other)->getItems();
  if (items.size() != other_items.size()) return false;
  for (auto& [key, val] : items) {
    // if (!other_items.count(key)) return false; // déjà contrôlé ?
    if (!other_items.at(key)->equals(items.at(key))) return false;
  }
  return true;
}

bool JsonArray::equals(const JsonValue* other) const {
  if (other == this) return true;
  if (other->type() != JsonType::array) return false;
  auto other_elements = ((JsonArray*)other)->getElements();
  if (elements.size() != other_elements.size()) return false;
  for (size_t i = 0; i < elements.size(); i++) {
    if (!elements[i]->equals(other_elements[i])) return false;
  }
  return true;
}

/*------------------------------------------------------------------------------
                        implémentation de bsize()
-------------------------------------------------------------------------------*/
uint32_t JsonObject::bsize() const {
  uint32_t sz = 5;  // 4 octets au départ pour la taille int32 du document
                    // Object + 1 pour l'octet null final en fin d'Object
  if (items.empty()) return 5;
  for (auto& [key, value] : items) {
    // 1 octet pour le codage du type de la valeur associée + 4 octets pour le
    // nobmre int32 de caractères dans la clé
    sz += 5 + key.size() * sizeof(char) + value->bsize();
  }
  return sz;  // + 1 pour l'octet null final
}

uint32_t JsonArray::bsize() const {
  uint32_t sz = 5;  // 4 octets au départ pour la taille int32 de l'Array + 1
                    // pour l'octet null final en fin d'Array
  if (elements.empty()) return 5;
  for (auto v : elements) {
    sz +=
        1 +
        v->bsize();  // 1 octet pour le codage du type de la valeur de l'élément
  }
  return sz;
}

uint32_t JsonString::bsize() const {
  return 4 +
         v_string.size() * sizeof(char);  // 4 = sizeof(int32) pour enregistrer
                                          // la longueur de la chaîne
}

uint32_t JsonNumber::bsize() const {
  return 8;  // 8 octets - 64-bits binary floating point
}

uint32_t JsonTrue::bsize() const { return 0; }

uint32_t JsonFalse::bsize() const { return 0; }

uint32_t JsonNull::bsize() const { return 0; }

/*------------------------------------------------------------------------------
                        implémentation de write(std::ostream& w)
-------------------------------------------------------------------------------*/
std::ostream& JsonValue::write(std::ostream& w) const {
  std::cout << "sauvegarde impossible : la valeur à sauvegarder n'est pas un "
               "document de type jsonObject.\n";
  return w;
}

std::ostream& JsonObject::write(std::ostream& w) const {
  uint32_t sz = bsize();
  w.write((char*)&sz, 4);  // int32 : taille du document en octets
  if (!items.empty()) {
    char bsonCode{0};
    uint32_t sz_key{0};
    for (auto& [key, value] : items) {
      bsonCode = value->bsonCode();
      w.write((char*)&bsonCode, 1);
      sz_key = key.size();
      w.write((char*)&sz_key,
              4);  // bInt32   écriture du nombre de caractères la clé
      w.write((char*)key.c_str(),
              key.size() * sizeof(char));  // écriture de la clé
      value->write(w);                     // écriture de la valeur
    }
  }
  char fin_doc = 0;
  w.write((char*)&fin_doc, 1);  // 1 byte de codage fin de l'object "\x00"  */
  return w;
}

std::ostream& JsonArray::write(std::ostream& w) const {
  uint32_t sz = bsize();
  char bsonCode{0};
  w.write((char*)&sz, 4);  // int32 : taille du tableau en octets
  for (auto& value : elements) {
    bsonCode = value->bsonCode();
    w.write((char*)&bsonCode, 1);
    value->write(w);
  }
  char fin_doc =
      0;  // sera lu en tant que bsonCode() == 0 indiquant la fin du tableau
  w.write((char*)&fin_doc, 1);  // 1 byte de codage fin du tableau "\x00"  */
  return w;
}

std::ostream& JsonString::write(std::ostream& w) const {
  uint32_t sz = v_string.size();
  w.write((char*)&sz, 4);  // int32
  w.write((char*)v_string.c_str(), sz * sizeof(char));
  return w;
}

std::ostream& JsonNumber::write(std::ostream& w) const {
  w.write((char*)&v_number, 8);
  return w;
}

std::ostream& JsonFalse::write(std::ostream& w) const { return w; }

std::ostream& JsonTrue::write(std::ostream& w) const { return w; }

std::ostream& JsonNull::write(std::ostream& w) const { return w; }

/*------------------------------------------------------------------------------
                        implémentation de read(std::istream& w)
-------------------------------------------------------------------------------*/

JsonObject* BSON::readDocument(std::istream& r) {
  //  std::cout << r.tellg() << std::endl;
  r.seekg(4, std::ios::cur);
  //    int sz{0};
  //    r.read((char*) &sz, 4); // taille du document en int32.
  auto obj = new JsonObject();
  char bsonCode{};
  int nb_charKey{};
  while (true) {
    r.read((char*)&bsonCode, 1);  // lecture de l'octet bsonCode indiquant le
                                  // type de l'item courant à lire;
    if (bsonCode == 0)
      return obj;  // code 0 de fin d'objet atteint. Terminé, document lu
    r.read((char*)&nb_charKey, 4);  // lecture du nombre de caractères de la clé
    char buffer[nb_charKey + 1];
    r.read((char*)buffer, nb_charKey * sizeof(char));
    buffer[nb_charKey] = 0;   // ajout du 0 fin de chaîne pour la clé
    std::string key{buffer};  // création de la clé en std::string
    JsonValue* v{};  // début de récupération de la valeur associée à la clé
    switch ((JsonType)bsonCode) {
      case JsonType::number:
        double d;
        r.read((char*)&d, 8);
        v = new JsonNumber(d);
        break;
      case JsonType::string:
        v = new JsonString();
        v->read(r);
        break;
      case JsonType::object:
        v = BSON::readDocument(r);  // embedded doc
        break;
      case JsonType::array:
        v = new JsonArray();  // embedded array
        v->read(r);
        break;
      case JsonType::ko:
        v = new JsonFalse();
        break;
      case JsonType::ok:
        v = new JsonTrue();
        break;
      case JsonType::null:
        v = new JsonNull();
        break;
    }
    obj->setValue(key, v);
  }
}

std::istream& JsonValue::read(std::istream& r) { return r; }

std::istream& JsonArray::read(std::istream& r) {
  r.seekg(4, std::ios::cur);
  //    int sz;
  //    r.read((char*) &sz, 4);
  while (true) {
    char bsonCode;
    r.read((char*)&bsonCode, 1);
    if (bsonCode == 0)
      return r;  // indicateur "0x00" de fin de tableau rencontré
    JsonValue* value{};
    switch ((JsonType)bsonCode) {
      case JsonType::number:
        double d;
        r.read((char*)&d, 8);
        value = new JsonNumber(d);
        break;
      case JsonType::string:
        value = new JsonString();
        value->read(r);
        break;
      case JsonType::object:
        value = BSON::readDocument(r);
        break;
      case JsonType::array:
        value = new JsonArray();
        value->read(r);
        break;
      case JsonType::ko:
        value = new JsonFalse();
        break;
      case JsonType::ok:
        value = new JsonTrue();
        break;
      case JsonType::null:
        value = new JsonNull();
        break;
    }
    this->add(value);
  }
  return r;
}

std::istream& JsonString::read(std::istream& r) {
  int sz;
  r.read((char*)&sz, 4);  // 4 = sizeof (bInt32)
  char buffer[sz + 1];
  r.read((char*)buffer, sz * sizeof(char));
  buffer[sz] = 0;
  v_string = buffer;
  return r;
}
