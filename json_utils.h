/*------------------------------------------------------------------------------
    json_utils.h
    Implémentation d'un parseur JSON.
    sviosdi - dernière mise à jour le
-------------------------------------------------------------------------------*/

#ifndef SVL_JSON_UTILS_H
#define SVL_JSON_UTILS_H

#include <string>

namespace sl {
std::string escapeChar(const std::string& s);
std::string showDouble(const double& d);
}  // namespace sl

#endif  // SVL_JSON_UTILS_H
