TEMPLATE = app
CONFIG += console c++20
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        json.cpp \
        jsonArray.cpp \
        jsonObject.cpp \
        json_utils.cpp \
        main.cpp \
        parser.cpp

HEADERS += \
	json.h \
	json_utils.h \
	parser.h
