#include <stdexcept>

#include "json.h"
using namespace sl;

JsonObject::~JsonObject() {
  for (auto& it : items) {
    if (it.second) delete it.second;
  }
}
void JsonObject::setValue(const std::string& key, JsonValue* val) {
  auto it = items.find(key);
  if (it != items.end()) {
    delete it->second;
    it->second = val;
  } else
    items[key] = val;
}

JsonValue* JsonObject::getValue(const std::string& key) const {
  auto it = items.find(key);
  if (it != items.end()) {
    return it->second;
  } else
    throw std::invalid_argument(
        "tentative d'accès à une clé d'un objet JSON non existante dans cet "
        "objet.");
}

bool JsonObject::contains(const std::string& key) const {
  auto it = items.find(key);
  return (it != items.end());
}

ObjectAccessor JsonObject::operator[](std::string key) {
  auto it = items.find(key);
  if (it != items.end()) {
    return {this, *(it->second), key};
  } else {
    return {this, ObjectAccessor::nokey, key};
  }
}

std::string JsonObject::stringify() const {
  if (items.empty()) return "{}";
  std::string str = "{";
  bool begin = true;
  for (auto& it : items) {
    if (!begin)
      str += ",";
    else
      begin = false;
    str += "\"" + it.first + "\":" + it.second->stringify();
  }
  return str + "}";
}

std::string JsonObject::prettyStringify(int ident) const {
  if (output == Output::bash || output == Output::simple) {
    std::string beg = output == Output::bash ? "\e[38;5;220m" : "";
    std::string end = output == Output::bash ? "\e[0m" : "";
    if (items.empty()) return beg + "{}" + end;
    std::string str;
    std::string white;
    std::string small_white;
    for (int i = 0; i < ident; i++) white += " ";
    for (int i = 0; i < ident - 2; i++) small_white += " ";
    str += beg + "{" + end + "\n";
    bool begin = true;
    for (auto& it : items) {
      if (!begin)
        str += ",\n";
      else
        begin = false;
      str += white + "  " + sl::escapeChar(it.first) + " : ";
      str += it.second->prettyStringify(ident + 2);
    }
    str += "\n";
    str += white + beg + "}" + end;
    return str;
  }
  return "";
}
