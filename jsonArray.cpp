#include <iostream>
#include <stdexcept>

#include "json.h"
using namespace sl;

// extern Output JsonValue::output;

JsonArray::~JsonArray() {
  for (auto& e : elements) {
    if (e) delete e;
  }
}

JsonValue* JsonArray::at(size_t i) {
  if (i >= elements.size())
    throw std::out_of_range(
        "tentative d'accès à un élément de tableau JSON hors de la plage "
        "d'accès.");
  else
    return elements[i];
}

void JsonArray::setAt(size_t i, JsonValue* v) {
  if (elements[i]) delete elements[i];
  elements[i] = v;
}

ArrayAccessor JsonArray::operator[](size_t idx) { return {*this, idx}; }

std::string JsonArray::stringify() const {
  if (elements.empty()) return "[]";
  std::string str;
  str += "[";
  bool begin = true;
  for (auto el : elements) {
    if (!begin)
      str += ",";
    else
      begin = false;
    str += el->stringify();
  }
  str += "]";
  return str;
}

std::string JsonArray::prettyStringify(int ident) const {
  if (output == Output::bash || output == Output::simple) {
    std::string beg = output == Output::bash ? "\e[38;5;220m" : "";
    std::string end = output == Output::bash ? "\e[0m" : "";
    if (elements.empty()) return beg + "[]" + end;
    std::string str;
    std::string white;
    std::string small_white;
    for (int i = 0; i < ident; i++) white += " ";
    for (int i = 0; i < ident - 2; i++) small_white += " ";
    str += beg + "[" + end + "\n";
    for (size_t i = 0; i < elements.size(); i++) {
      str += white + "  " + elements[i]->prettyStringify(ident + 2);
      str += (i != elements.size() - 1) ? ",\n" : "\n";
    }
    str += white + beg + "]" + end;
    return str;
  }
  return "";
}

void JsonArray::add(JsonValue* v) { elements.push_back(v); }

void JsonArray::add(const std::string& str) {
  elements.push_back(new JsonString(str));
}

void JsonArray::add(double d) { elements.push_back(new JsonNumber(d)); }
void JsonArray::add(JsonType tp) {
  switch (tp) {
    case JsonType::ok:
      elements.push_back(new JsonTrue());
      break;
    case JsonType::ko:
      elements.push_back(new JsonFalse());
      break;
    case JsonType::null:
      elements.push_back(new JsonNull());
      break;
    default:
      std::cerr << "json add element error: Seuls les valeur de type "
                   "JsonType::ok, ko et null peuvent être ajoutés par"
                   " leur type à un tableau par l'opérateur =. Sinon "
                   "l'ajout sera sans effet."
                << std::endl;
      break;
  }
}
