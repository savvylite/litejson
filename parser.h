/*------------------------------------------------------------------------------
    parser.h
    Implémentation d'un parseur JSON.
    Licence: GPLv3
    sviosdi - dernière mise à jour le 23.12.2022
-------------------------------------------------------------------------------*/

#ifndef SVL_PARSER_H
#define SVL_PARSER_H
#include <fstream>

#include "json.h"

namespace sl {

struct ParserJson {
  ParserJson() : ch{' '}, at{0} {}
  virtual char _next(char c = 0) = 0;
  virtual void _error(std::string) = 0;
  JsonValue* _value();
  JsonObject* _object();
  JsonArray* _array();
  std::string _string();
  JsonValue* _number();
  JsonValue* _word();
  void _white();
  char ch;
  uint32_t at;
};

struct StringParserJson : ParserJson {
  StringParserJson(const std::string& str) : ParserJson(), text{str.c_str()} {}
  void _error(std::string) override;
  char _next(char c = 0) override;
  const char* text;
};

struct StreamParserJson : ParserJson {
  StreamParserJson(std::ifstream& stm) : ParserJson(), stream{stm} {}
  void _error(std::string) override;
  char _next(char c = 0) override;
  std::ifstream& stream;
};

class ParseException {
 public:
  ParseException(std::string m, size_t at, const char* text);
  void what();

 private:
  std::string message;
  uint32_t eat;
  std::string etext;
};

}  // namespace sl

#endif  // SVL_PARSER_H
