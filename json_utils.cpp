#include "json_utils.h"

std::string sl::escapeChar(const std::string& s) {
  std::string r;
  for (char c : s) {
    switch (c) {
      case '\n':
        r += "\\n";
        break;
      case L'\r':
        r += "\\r";
        break;
      case L'\t':
        r += "\\t";
        break;
      case L'\b':
        r += "\\b";
        break;
      case L'\v':
        r += "\\v";
        break;
      case L'\f':
        r += "\\f";
        break;
      case L'\a':
        r += "\\a";
        break;
      default:
        r += c;
    }
  }
  return r;
}

std::string sl::showDouble(const double& d) {
  std::string str = std::to_string(d);
  for (uint i = 0; i < str.size(); i++) {
    if (str[i] == ',') {
      str[i] = '.';
      break;
    }
  }
  unsigned int i = str.size() - 1;
  while (str[i] == '0') i--;
  return (str[i] == '.') ? std::string(str, 0, i) : std::string(str, 0, i + 1);
}
