
#include <fstream>
#include <iostream>

#include "json.h"
using std::cout;
using std::endl;
using std::string;
using namespace sl;

int main() {
  setlocale(LC_CTYPE, "");

  string toParse =
      R"({"projet":"liteJSON", "stars":5, "embedded":{"test":"fine", "tab":[], "ok":true, "level":5}})";
  std::ofstream out("doc");
  auto v = JSON::parse(toParse);
  v->write(out);
  out.close();
  delete v;

  std::ifstream in("doc");
  auto v2 = BSON::readDocument(in);
  in.close();

  v2->setOutput(Output::bash);
  cout << v2->prettyStringify() << endl;

  delete v2;
  return 0;
}
